FROM  joeferner/node-java 

RUN mkdir -p /app
COPY . /app
WORKDIR /app

RUN npm install jmx
RUN npm install
CMD ["npm", "start"]
